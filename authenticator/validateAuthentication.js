const joi = require('joi');

const validateAuth = async (req, res, next) => {
  const schema = joi.object({
    username: joi.string()
        .alphanum()
        .min(3)
        .max(30)
        .required(),
    password: joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$'))
        .required(),
  });

  const result = schema.validate(req.body);
  const {error} = result;
  const valid = error == null;
  if (!valid) {
    res.status(400).json({
      message: 'Invalid data',
    });
  }
  next();
};

module.exports = {
  validateAuth,
};
