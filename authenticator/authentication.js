const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
require('dotenv').config();
const JWT_SECRET = process.env.JWT_SECRET;
const {User} = require('../models/userModel');

const registration = async (req, res) => {
  const {username, password} = req.body;

  try {
    if (username && password) {
      let user = await User.findOne({username});
      if (user) {
        res.status(400).json({message: 'User is already exist!'});
      } else {
        user = new User({
          username,
          password: await bcrypt.hash(password, 10),
        });

        await user.save();
        res.status(200).json({message: 'Success'});
      }
    } else {
      res.status(400).json({message: 'Enter all credentials'});
    }
  } catch (error) {
    return res.status(500).json({message: 'Server error'});
  }
};

const login = async (req, res) => {
  try {
    const {username, password} = req.body;
    const user = await User.findOne({username});

    if (!user && !password) {
      res.status(400).json({message: 'Enter your credentials!'});
    }

    if (!user) {
      return res
          .status(400)
          .json({message: `No user with username '${username}' found!`});
    }

    if (!(await bcrypt.compare(password, user.password))) {
      return res.status(400).json({message: `Invalid password!`});
    }

    const token = jwt.sign(
        {username: user.username, _id: user._id},
        JWT_SECRET,
    );
    res.status(200).json({message: 'Success', jwt_token: token});
  } catch (error) {
    return res.status(500).json({message: 'Server error'});
  }
};

module.exports = {
  registration,
  login,
};
