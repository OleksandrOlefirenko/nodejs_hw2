const mongoose = require('mongoose');
const {Note} = require('./noteModel');

// eslint-disable-next-line new-cap
const userSchema = mongoose.Schema(
    {
      username: {
        type: String,
        required: true,
        uniq: true,
      },
      password: {
        type: String,
        required: true,
      },
      createdDate: {
        type: Date,
        default: Date.now(),
      },
    },
    {
      collection: 'users',
    },
);

userSchema.virtual('notes', {
  ref: 'Note',
  localField: '_id',
  foreignField: 'userId',
});

userSchema.pre('remove', async function(next) {
  const user = this;
  await Note.deleteMany({userId: user.id});
  next();
});

module.exports.User = mongoose.model('User', userSchema);
