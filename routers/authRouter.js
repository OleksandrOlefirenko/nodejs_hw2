const express = require('express');
const router = new express.Router();
const {registration, login} = require('../authenticator/authentication');
const {asyncWrapper} = require('../middlewares/helpers');
const {validateAuth} = require('../authenticator/validateAuthentication');

router.post('/register',
    asyncWrapper(validateAuth),
    asyncWrapper(registration));
router.post('/login', asyncWrapper(login));

module.exports = router;
