const express = require('express');
const router = new express.Router();
const auth = require('../middlewares/auth');
const {Note} = require('../models/noteModel');

router.post('/', auth, async (req, res) => {
  try {
    const text = req.body.text;

    if (typeof text !== 'string') {
      res.status(400).json({message: 'text of note must have a string type'});
    }

    const note = new Note({
      userId: req.user._id,
      text: req.body.text,
    });

    await note.save();
    res.status(200).json({message: 'Success'});
  } catch (error) {
    res.status(400).json({message: error.message});
  }
});

router.get('/', auth, async (req, res) => {
  try {
    await req.user.populate({
      path: 'notes',
      options: {
        skip: parseInt(req.query.offset),
        limit: parseInt(req.query.limit),
      },
    }).depopulate('__v').execPopulate();
    res.status(200).json({notes: [...req.user.notes]});
  } catch (error) {
    res.status(400).json({message: error.message});
  }
});

router.get('/:id', auth, async (req, res) => {
  try {
    const _id = req.params.id;
    const note = await Note.findOne({_id, userId: req.user._id});

    if (!note) {
      res.status(400).json({message: 'No such task'});
    }

    res.status(200).send({note: note});
  } catch (error) {
    res.status(400).json({message: 'No such task with this id'});
  }
});

router.put('/:id', auth, async (req, res) => {
  try {
    const _id = req.params.id;
    const note = await Note.findOne({_id, userId: req.user._id});

    if (!note) {
      res.status(400).json({message: 'No such task'});
    }

    if (!req.body.text) {
      res.status(400).json({message: 'Text field can bot be empty'});
    } else {
      if (note.text === req.body.text) {
        res.status(400)
            .json({message: 'Please write a new description of note'});
      } else {
        note.text = req.body.text;
        await note.save();
        res.status(200).json({message: 'Success'});
      }
    }
  } catch (error) {
    res.status(400).json({message: 'No such task with this id'});
  }
});

router.patch('/:id', auth, async (req, res) => {
  try {
    const _id = req.params.id;
    const note = await Note.findOne({_id, userId: req.user._id});

    if (!note) {
      res.status(400).json({message: 'No such task'});
    }

    note.completed = !note.completed;
    await note.save();
    res.status(200).json({message: 'Success'});
  } catch (error) {
    res.status(400).json({message: 'No such task with this id'});
  }
});

router.delete('/:id', auth, async (req, res) => {
  try {
    const _id = req.params.id;
    const note = await Note.findOne({_id, userId: req.user._id});
    if (!note) {
      res.status(400).json({message: 'No such task'});
    }
    await Note.findOneAndDelete({
      _id,
      userId: req.user._id,
    });
    res.status(200).json({message: 'Success'});
  } catch (error) {
    res.status(400).json({message: 'No such task with this id'});
  }
});

module.exports = router;
