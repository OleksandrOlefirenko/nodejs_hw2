const express = require('express');
const bcrypt = require('bcrypt');
const router = new express.Router();
const auth = require('../middlewares/auth');

router.get('/', auth, async (req, res) => {
  try {
    if (req.user) {
      res.status(200).json({
        user: {
          _id: req.user._id,
          username: req.user.username,
          createdDate: req.user.createdDate,
        },
      });
    } else {
      res.status(400).json({message: 'Please autheticate!'});
    }
  } catch (error) {
    res.status(500).json({message: error.message});
  }
});

router.delete('/', auth, async (req, res) => {
  try {
    await req.user.remove();
    res.status(200).json({message: 'Success'});
  } catch (error) {
    res.status(400).json({message: error.message});
  }
});

router.patch('/', auth, async (req, res) => {
  try {
    const user = req.user;
    const {oldPassword, newPassword} = req.body;

    if (oldPassword && newPassword) {
      if (!(await bcrypt.compare(oldPassword, user.password))) {
        return res.status(400).json({message: `Wrong password!`});
      } else {
        if (newPassword != oldPassword) {
          user.password = await bcrypt.hash(newPassword, 10);
          await user.save();
          res.status(200).json({message: 'Success'});
        }
      }
    } else {
      res.status(400).json({message: 'Enter all needed credentials!'});
    }
  } catch (error) {
    res.status(500).json({message: error.message});
  }
});

module.exports = router;
