const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
require('dotenv').config();
const app = express();
const port = process.env.PORT || 8080;

const authRouter = require('./routers/authRouter');
const noteRouter = require('./routers/noteRouter');
const userRouter = require('./routers/userRouter');

app.use(morgan('tiny'));
app.use(express.json());
app.use('/api/auth', authRouter);
app.use('/api/notes', noteRouter);
app.use('/api/users/me', userRouter);
app.use((err, req, res, next) => {
  res.status(500).json({message: err.message});
});
app.use('', (req, res) => {
  res.status(500).json({message: 'Please write correct url'});
});

const start = async () => {
  try {
    await mongoose.connect(
        'mongodb+srv://testuser:testuser@cluster0.zaszh.mongodb.net/cluster0?retryWrites=true&w=majority',
        {
          useNewUrlParser: true,
          useUnifiedTopology: true,
          useFindAndModify: false,
          useCreateIndex: true,
        },
    );
  } catch (error) {
    console.log(error.message);
  }

  app.listen(port);
};

start();
